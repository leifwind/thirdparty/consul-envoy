FROM consul:1.10.1
FROM envoyproxy/envoy:v1.19.1
COPY --from=0 /bin/consul /bin/consul 
ENTRYPOINT ["dumb-init", "consul", "connect", "envoy"]
